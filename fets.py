from time import sleep
import datetime
import board
import os
import RPi.GPIO as gpio
import adafruit_mcp4728 as MCP
import adafruit_ads1x15.ads1115 as ADS
import matplotlib.pyplot as plt
from adafruit_ads1x15.analog_in import AnalogIn

class profiler:
    # Considerar trocar alguns desses argumentos por kwargs
    I2C = board.I2C()
    mcp = MCP.MCP4728(I2C)
    MCP_SLEEP_TIME = 1
    MCP_PIN_MAP = {'gate': mcp.channel_a.raw_value,
                   'ref': mcp.channel_b.raw_value,
                   'drain': mcp.channel_c.raw_value}
    MULTIPLEX_SLEEP_TIME = 1
    I_GAIN = 148
    FULL_VREF_RAW = 4095
    ZERO = int(FULL_VREF_RAW / 2)
    print('ZERO:', ZERO)
    '''
    elf.COLOR_LIST = [
        "#cc241d",
        "#98971a",
        "#d79921",
        "#458588",
        "#b16286",
        "#689d6a",
        "#7c6f64",
    ]
    '''
    def __init__(self, mux_pins, r_shunt, data_rate=128, gain=1, AMS=5, sleep_time=1):
        gpio.setup((mux_pins['A'], mux_pins['B']), gpio.OUT)
        gpio.setup(mux_pins['INH'], gpio.OUT, initial=gpio.HIGH)
        self.mux_pins = mux_pins
        self.AMS = AMS
        self.sleep_time = sleep_time


        # ADS config
        ads = self.ads = ADS.ADS1115(self.I2C, data_rate=data_rate, gain=gain)
        self.dif01 = AnalogIn(ads, ADS.P0, ADS.P1)  # Canal 0 - Canal 1 (GND)
        self.dif23 = AnalogIn(ads, ADS.P2, ADS.P3)  # Canal 2 - Canal 3 (GND)

        # MCP config
        self.zeroMCP()
        self.full_vref_real = self.getFullVref()
        self.offset = self.getOffset()
        self.zero_offset = self.ZERO - int(
            self.offset * self.FULL_VREF_RAW / self.full_vref_real
        )
        print("zero-offset:", self.zero_offset)
        # Amperimetro config
        self.r_shunt = r_shunt
        
    def zeroMCP(self):
        mcp = self.mcp
        mcp.channel_a.raw_value = self.ZERO
        mcp.channel_b.raw_value = self.ZERO
        mcp.channel_c.raw_value = self.ZERO
        mcp.channel_d.raw_value = self.ZERO
        mcp.channel_a.vref = MCP.Vref.VDD
        mcp.channel_b.vref = MCP.Vref.VDD
        mcp.channel_c.vref = MCP.Vref.VDD
        mcp.channel_d.vref = MCP.Vref.VDD
        mcp.save_settings()
        sleep(self.MCP_SLEEP_TIME)

    def getOffset(self):
        self.zeroMCP()

        # Dividir essa linha
        offset = (self.readADSVoltage("gate", "source") + self.readADSVoltage("drain", "source")) / 2
        print("Offset:", offset)
        self.setMultiplex("OFF")
        return offset

    def getFullVref(self):
        self.mcp.channel_a.raw_value = self.FULL_VREF_RAW
        self.mcp.channel_b.raw_value = self.ZERO
        self.mcp.channel_c.raw_value = 0

        sleep(self.MCP_SLEEP_TIME)

        v_full = self.readADSVoltage("gate", "source")        
        v_zero = self.readADSVoltage("drain", "source")
        full_vref_real = v_full - v_zero

        print("Vdd:", full_vref_real, "V")

        self.setMultiplex("OFF")
        self.zeroMCP()

        return full_vref_real

    def setMultiplex(self, state):
        mux_pins = self.mux_pins
        if state == "OFF":
            gpio.output(mux_pins['INH'], gpio.HIGH)
        elif state == "V":
            gpio.output((mux_pins['INH'],
                        mux_pins['A'],
                        mux_pins['B']),
                        (gpio.LOW,
                         gpio.HIGH,
                         gpio.LOW))
        elif state == "I":
            gpio.output((mux_pins['INH'],
                        mux_pins['A'],
                        mux_pins['B']),
                        (gpio.LOW,
                         gpio.LOW,
                         gpio.LOW)
                        )
        sleep(self.MULTIPLEX_SLEEP_TIME)

    def setVoltage(self, voltage, pin):
        mcp = self.mcp
        voltage = self.zero_offset + int(
            voltage * self.FULL_VREF_RAW / self.full_vref_real
        )

        # Talvez valha a pena usar um dicionário
        if pin == "gate":
            mcp.channel_a.raw_value = voltage
        elif pin == "ref":
            mcp.channel_b.raw_value = voltage
        elif pin == "drain":
            mcp.channel_c.raw_value = voltage
        elif pin == "all":
            mcp.channel_a.raw_value = voltage
            mcp.channel_b.raw_value = voltage
            mcp.channel_c.raw_value = voltage

        sleep(self.MCP_SLEEP_TIME+self.sleep_time)

    def readADSCurrent(self, pin):
        self.setMultiplex("I")
        dif01 = dif23 = 0
        
        if pin == "drain":
            for i in range(self.AMS):
                dif01 += self.dif01.voltage
            return dif01 / (self.r_shunt[pin] * self.I_GAIN * self.AMS)

        elif pin == "gate":
            for i in range(self.AMS):
                dif23 += self.dif23.voltage
            return dif23 / (self.r_shunt[pin] * self.I_GAIN * self.AMS)

    def readADSVoltage(self, pin, read_type="out"):
        self.setMultiplex("V")
        dif01 = dif23 = 0

        if read_type == "out":
            if pin == "drain":
                for i in range(self.AMS):
                    dif01 += self.dif01.voltage
                return (dif01 / self.AMS) - (self.r_shunt[pin] * self.readADSCurrent(pin))
            elif pin == "gate":
                for i in range(self.AMS):
                    dif23 += self.dif23.voltage
                return (dif23 / self.AMS) - (self.r_shunt[pin] * self.readADSCurrent(pin))

        elif read_type == "source":
            if pin == "drain":
                for i in range(self.AMS):
                    dif01 += self.dif01.voltage
                return dif01 / self.AMS
            elif pin == "gate":
                for i in range(self.AMS):
                    dif23 += self.dif23.voltage
                return dif23 / self.AMS

    def queryCurrent(self, voltage, pin):
        self.setVoltage(voltage, pin)
        return (self.readADSCurrent(pin), self.readADSVoltage(pin))

    def caracterize(
        self,
        x_space,
        curve_space,
        x_pin,
        curve_pin,
        result_folder="resultados",
        scale={'Vgate': 1, 'Vdrain': 1,
               'Igate': 1, 'Idrain': 1}
    ):
        # x_space: conjunto de valores de tensão para a varredura
        # curve_space: conjunto de valores de tensão fixa
        # x_pin e curve_pin: 'gate' ou 'drain'
        sufix={}
        sufix['Vgate'] = self.getSufix(scale['Vgate'], 'V')
        sufix['Vdrain'] = self.getSufix(scale['Vdrain'], 'V')
        sufix['Igate'] = self.getSufix(scale['Igate'], 'A')
        sufix['Idrain'] = self.getSufix(scale['Idrain'], 'A')

        sufixes = (x_pin,
                   sufix['V'+x_pin],
                   x_pin,
                   sufix['I'+x_pin],
                   curve_pin,
                   sufix['I'+curve_pin]
                   )
                   
        timedate = datetime.datetime.now()
        time_now = timedate.strftime("%Y-%m-%d-%H:%M:%S")
        os.mkdir("resultados/%s" % time_now)
        print("curve_space:", curve_space)

        for v_curve in curve_space:
            self.setVoltage(v_curve, curve_pin)
            print("v_curve:", v_curve)
            v_curve_scaled = scale['V'+curve_pin] * self.readADSVoltage(curve_pin)
            print("v_curve_scaled", v_curve_scaled)

            filename = "resultados/%s/%s%d%s.csv" % (
                time_now,
                curve_pin,
                v_curve_scaled,
                sufix['V'+curve_pin],
            )

            with open(filename, "w") as f:

                # considere colocar essa string numa variável
                f.write("V%s(%s):,%.4f\n" % (curve_pin, sufix['V'+curve_pin], v_curve_scaled))
                print("%s(%s):%.4f\n" % (curve_pin, sufix['V'+curve_pin], v_curve_scaled))

                f.write(
                    "V%s(%s),I%s(%s),I%s(%s)\n"
                    % sufixes
                )
                print(
                    "| V%s(%s) | I%s(%s) | I%s(%s) |"
                    % sufixes
                )

                #I_buffer = []
                #V_buffer = []

                for x in x_space:
                    Ix, Vx = self.queryCurrent(x, x_pin)
                    Icurve = scale['I'+curve_pin] * self.readADSCurrent(curve_pin)
                    Ix *= scale['I'+x_pin]
                    Vx *= scale['V'+x_pin]

                    #I_buffer.append(Ix)
                    #V_buffer.append(Vx)

                    f.write("%.4f,%.4f,%.4f\n" % (Vx, Ix, Icurve))
                    print("| %.4f | %.4f | %.4f |" % (Vx, Ix, Icurve))

    def plotGen(self, x_pin, v_sufix, i_sufix):
        fig, ax = plt.subplots()
        ax.set_xlabel("V%s(%s)" % (x_pin, v_sufix))
        ax.set_ylabel("I%s(%s)" % (x_pin, i_sufix))
        ax.grid(True)
        plt.pause(2)
        return fig, ax

    @staticmethod
    def getSufix(scale, unity):
        if scale == 1:
            return unity
        elif scale == 1000:
            return "m" + unity
        elif scale == 1000000:
            return "u" + unity
        elif scale == 1000000000:
            return "n" + unity
